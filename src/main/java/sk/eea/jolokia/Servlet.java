package sk.eea.jolokia;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

/**
 * Statically lined servlet by atlassian-plugin definition delegetas all functionality to ManagedServlet.
 */
public class Servlet extends HttpServlet {
    private final Service service;

    public Servlet(Service service) {
        this.service = service;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        service.initServlet(config);
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        service.getServlet().service(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        service.destroyServlet();
        super.destroy();
    }
}
