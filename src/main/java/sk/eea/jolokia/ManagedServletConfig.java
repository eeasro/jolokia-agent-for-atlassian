package sk.eea.jolokia;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

public class ManagedServletConfig implements ServletConfig {
    private final Map<String, String> settings;
    private final ServletContext servletContext;

    ManagedServletConfig(final ServletContext servletContext, final Map<String, String> settings) {
        this.settings = settings;
        this.servletContext = servletContext;
    }

    @Override
    public String getServletName() {
        return servletContext.getServletContextName();
    }

    @Override
    public ServletContext getServletContext() {
        return servletContext;
    }

    @Override
    public String getInitParameter(String s) {
        return settings.get(s);
    }

    @Override
    public Enumeration getInitParameterNames() {
        return Collections.enumeration(settings.keySet());
    }
}


