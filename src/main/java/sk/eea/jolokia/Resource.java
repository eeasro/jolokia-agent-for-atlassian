package sk.eea.jolokia;

import com.atlassian.plugins.rest.common.security.jersey.SysadminOnlyResourceFilter;
import com.atlassian.sal.api.user.UserManager;
import com.sun.jersey.spi.container.ResourceFilters;
import org.codehaus.jackson.map.ObjectMapper;
import org.jolokia.restrictor.PolicyRestrictor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

@Path("/")
@ResourceFilters(SysadminOnlyResourceFilter.class)
public class Resource {
    private final Service service;

    public Resource(@SuppressWarnings("unused") UserManager userManager, Service service) {
        this.service = service;
    }

    @GET
    @Path("access")
    @Produces(MediaType.TEXT_XML)
    public Response getAccessXML(@Context HttpServletRequest request) {
        String access_xml = service.getAccessXml();
        return Response.ok(access_xml != null ? access_xml : service.getDefaultAccessXml()).build();
    }

    @PUT
    @Path("access")
    @Consumes(MediaType.TEXT_XML)
    public Response putAccessXML(final String access_xml, @Context HttpServletRequest request) {
        if ("".equals(access_xml)) {
            // reseting on empty value
            service.setAccessXml(null);
        } else {
            // parsing provided configuration
            try {
                new PolicyRestrictor(new ByteArrayInputStream(access_xml.getBytes(StandardCharsets.UTF_8)));
            } catch (SecurityException e) {
                return Response.serverError().entity(e.getMessage()).build();
            }
            service.setAccessXml(access_xml);
        }
        try {
            service.resetServlet();
            return Response.status(Response.Status.ACCEPTED).build();
        } catch (ServletException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("system-config")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSystemSettings(@Context HttpServletRequest request) {
        return Response.ok(service.getSystemSettings()).build();
    }

    @GET
    @Path("config")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSettings(@Context HttpServletRequest request) {
        return Response.ok(service.getSettings()).build();
    }

    @PUT
    @Path("config")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putSettings(final String jsonSettings, @Context HttpServletRequest request) {
        try {
            service.setSettings(new ObjectMapper().readValue(jsonSettings, HashMap.class));
            service.resetServlet();
        } catch (IOException | ServletException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
        return Response.status(Response.Status.ACCEPTED).build();
    }

}