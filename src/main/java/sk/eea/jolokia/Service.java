package sk.eea.jolokia;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import java.util.ArrayList;
import java.util.Arrays;
import org.jolokia.config.ConfigKey;
import org.jolokia.restrictor.PolicyRestrictor;
import org.jolokia.util.NetworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Service implements LifecycleAware {
    private final Logger logger = LoggerFactory.getLogger(Service.class);
    private static final String KEY_ACCESS_XML = "plugin.jolokia.access.xml";
    private static final String KEY_CONFIG = "plugin.jolokia.config";
    private final PluginSettings globalSettings;
    private final ArrayList<ConfigKey> SETTINGS = new ArrayList<>(Arrays.asList(
            ConfigKey.AGENT_ID, ConfigKey.AGENT_DESCRIPTION,
            ConfigKey.DISCOVERY_ENABLED, ConfigKey.DISCOVERY_AGENT_URL,
            ConfigKey.MULTICAST_GROUP, ConfigKey.MULTICAST_PORT,
            ConfigKey.ALLOW_DNS_REVERSE_LOOKUP, ConfigKey.CANONICAL_NAMING,
            ConfigKey.DEBUG, ConfigKey.DEBUG_MAX_ENTRIES, ConfigKey.HISTORY_MAX_ENTRIES,
            ConfigKey.INCLUDE_STACKTRACE, ConfigKey.SERIALIZE_EXCEPTION, ConfigKey.ALLOW_ERROR_DETAILS,
            ConfigKey.MAX_COLLECTION_SIZE, ConfigKey.MAX_DEPTH, ConfigKey.MAX_OBJECTS,
            ConfigKey.POLICY_LOCATION));

    // TODO AplicationProperties are not available in Detector?
    static String appName = "?";
    static String appVersion = "?";
    static Map<String, String> extraInfo = null;
    private ManagedServlet servlet;
    private ServletConfig servletConfig;

    public Service(PluginSettingsFactory pluginSettingsFactory, ApplicationProperties applicationProperties) {
        globalSettings = pluginSettingsFactory.createGlobalSettings();
        appName = applicationProperties.getDisplayName();
        appVersion = applicationProperties.getVersion();
        extraInfo = new HashMap<>();
        extraInfo.put("baseUrl", applicationProperties.getBaseUrl());
        extraInfo.put("buildNumber", applicationProperties.getBuildNumber());
    }

    private PolicyRestrictor getPolicyRestrictor() {
        String access_xml = getAccessXml();
        if (access_xml != null) {
            try {
                return new PolicyRestrictor(new ByteArrayInputStream(access_xml.getBytes(StandardCharsets.UTF_8)));
            } catch (SecurityException e) {
                // FIXME store me error message
            }
        }
        return new PolicyRestrictor(getDefaultAccessXmlAsStream());
    }

    private InputStream getDefaultAccessXmlAsStream() {
        return this.getClass().getResourceAsStream("/jolokia-access.xml");
    }

    private String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    String getDefaultAccessXml() {
        try {
            return readFromInputStream(getDefaultAccessXmlAsStream());
        } catch (IOException e) {
            return "";
        }
    }

    String getAccessXml() {
        final String value = (String) globalSettings.get(KEY_ACCESS_XML);
        logger.debug("Providing stored access.xml: {}", value);
        return value;
    }

    void setAccessXml(String value) {
        if (value == null) {
            globalSettings.remove(KEY_ACCESS_XML);
            logger.debug("Clearing access.xml");
        } else {
            globalSettings.put(KEY_ACCESS_XML, value);
            logger.debug("Storing access.xml: {}", value);
        }
    }

    public Map<String,String> getSystemSettings() {
        Map<String, String > settings = getSettings();
        for (ConfigKey configKey: SETTINGS) {
            String value = System.getProperty("jolokia"+"."+configKey.getKeyValue());
            if (value != null) {
                settings.put(configKey.getKeyValue(), value);
                logger.debug("Applying system setting {}={}", configKey.getKeyValue(), value);
            }
        }
        return settings;
    }

    public Map<String,String> getSettings() {
        HashMap<String, String > settings = new HashMap<>();
        for (ConfigKey configKey: SETTINGS) {
            Object value = globalSettings.get(KEY_CONFIG +"."+configKey.getKeyValue());
            if (value != null) {
                settings.put(configKey.getKeyValue(), value.toString());
            }
        }
        return settings;
    }

    void setSettings(Map<String, String> settings) {
        for (String key: settings.keySet()) {
            if (ConfigKey.getGlobalConfigKey(key) != null) {
                final String ref = KEY_CONFIG + "." + key;
                if (settings.get(key) == null) {
                    globalSettings.remove(ref);
                } else {
                    globalSettings.put(ref, settings.get(key));
                }
            }
        }
    }

    HttpServlet getServlet() {
        return servlet;
    }

    void destroyServlet() {
        if (servlet != null) {
            servlet.destroy();
            servlet = null;
        }
    }

    void initServlet(ServletConfig config) throws ServletException {
        logger.trace("Entering initServlet");
        servletConfig = config;
        final Map<String, String> systemSettings = getSystemSettings();
        servlet = new ManagedServlet(getPolicyRestrictor());
        servlet.init(new ManagedServletConfig(servletConfig.getServletContext(), systemSettings));
    }

    void resetServlet() throws ServletException {
        if (servletConfig != null) {
            destroyServlet();
            initServlet(servletConfig);
        }
    }

    /**
     * Initialize servlet actively when jolokia.discoveryAgentUrl id provided.
     *
     * Servlet module is not initialized on start, we have to trigger by http get
     */
    @Override
    public void onStart() {
        logger.trace("Entering onStart");
        String discoveryAgentUrl = findAgentUrl();
        if (discoveryAgentUrl != null) {
            try {
                URL url = new URL(discoveryAgentUrl);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setConnectTimeout(5000);
                con.setReadTimeout(5000);
                final int responseCode = con.getResponseCode();
                if (responseCode > 299) {
                    logger.warn("Unexpected response "+ responseCode +" from Jolokia Agent at "+discoveryAgentUrl);
                } else {
                    logger.info("Jolokia Agent triggered at "+discoveryAgentUrl);
                }
                con.disconnect();
            } catch (IOException e) {
                logger.error("Error triggering Jolokia Agent at "+discoveryAgentUrl);
            }
        }
    }

    /**
     * Copied from AgentServlet
     */
    private String findAgentUrl() {
        // System property has precedence
        String url = System.getProperty("jolokia." + ConfigKey.DISCOVERY_AGENT_URL.getKeyValue());
        if (url == null) {
            url = System.getenv("JOLOKIA_DISCOVERY_AGENT_URL");
            if (url == null) {
                url = getSettings().get(ConfigKey.DISCOVERY_AGENT_URL.getKeyValue());
            }
        }
        return NetworkUtil.replaceExpression(url);
    }
}