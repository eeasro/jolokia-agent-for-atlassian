package sk.eea.jolokia;

import org.jolokia.http.AgentServlet;
import org.jolokia.restrictor.PolicyRestrictor;
import org.jolokia.util.LogHandler;

import javax.servlet.ServletConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManagedServlet extends AgentServlet {

    /**
     * Initializes servlet with configured policy restrictor
     */
    ManagedServlet(PolicyRestrictor policyRestrictor) {
        super(policyRestrictor);
    }

    @Override
    protected LogHandler createLogHandler(ServletConfig pServletConfig, final boolean pDebug) {
        return new LogHandlerImpl();
    }

    public static class LogHandlerImpl implements LogHandler {

        private final Logger logger = LoggerFactory.getLogger(ManagedServlet.class);

        @Override
        public void debug(String s) {
            logger.debug(s);
        }

        @Override
        public void info(String s) {
            logger.info(s);
        }

        @Override
        public void error(String s, Throwable throwable) {
            logger.error(s, throwable);
        }
    }
}
