# Jolokia Agent packaged for Atlassian products as P2 add-on.

Add-on packages [Jolokia Agent](https://jolokia.org) for all Atlassian (UPM capable) standalone products.

Agent provides HTTP endpoint for JMX at `$APP_BASE/plugins/servlet/jolokia`
This service could be consumed by various monitoring tools

- [check_jmx4perl](http://search.cpan.org/~roland/jmx4perl/scripts/check_jmx4perl) in Nagios based products
- [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) has built-in [module](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/jolokia) - tested with version 1.10.2
- [Elastic Metricbeat](https://www.elastic.co/products/beats/metricbeat) has built-in [module](https://www.elastic.co/guide/en/beats/metricbeat/master/metricbeat-module-jolokia.html)

Add-on is installable using UPM. Default `jolokia-access.xml` allows access using GET or POST from localhost for read, list and version operation without authorization.

```
# APP_BASE=http://localhost:5990/refapp
# curl -sS $APP_BASE/plugins/servlet/jolokia/version
{"request":{"type":"version"},"value":{"agent":"1.6.0","protocol":"7.2" ...
# curl -sS -X POST -H "Content-Type: application/json" -d '{"type": "version"}' $APP_BASE/plugins/servlet/jolokia
{"request":{"type":"version"},"value":{"agent":"1.6.0","protocol":"7.2" ...
```

## Configuration

For changing access configuration you have to download this definition to file, modify and upload the changed definition over REST as system administrator, see [documentation](https://jolokia.org/reference/html/security.html) for syntax.

```
# curl -u admin:admin -o my-jolokia-access.xml -s $APP_BASE/rest/jolokia/1.0/access
# edit my-jolokia-access.xml
# curl -u admin:admin -X PUT --header "Content-Type:text/xml" --data @my-jolokia-access.xml $APP_BASE/rest/jolokia/1.0/access
```

Logging of _Jolokia Agent_ is exposed using hosting Atlassian Application infrastructure as class `sk.eea.jolokia.ManagedServlet`. 
For debugging you have to set up level *DEBUG* in Jira and also set patameter *debug* (see below) to *true*. 
 
Following parameters are configurable in the servlet (as described in [Table 3.1 Servlet init parameters](https://jolokia.org/reference/html/agents.html))
* `agentId`, `agentDescription`
* `discoveryEnabled`, `discoveryAgentUrl`, `multicastGroup`, `multicastPort`
* `allowDnsReverseLookup`, `canonicalNaming`
* `debug`, `debugMaxEntries`, `historyMaxEntries`
* `includeStackTrace`, `serializeException`, `allowErrorDetails`
* `maxDepth`, `maxCollectionSize`, `maxObjects`

These parameters are configurable either using system properties, e.g. `-Djolokia.debug=true` in `CATALINA_OPTS` for running JVM
or application wide using dedicated `config` endpoint:
```
# curl -u admin:admin -X PUT -sS -H "Content-Type: application/json"  -d'{"debug":"true"}' $APP_BASE/rest/jolokia/1.0/config
# curl -u admin:admin -sS $APP_BASE/rest/jolokia/1.0/config
  {"debug":"true"}
```

## Roadmap

* configuration screen for add-on
* data-center support

## Changelog

### Release 0.8.0 - 2022-02-22

* configuration using system properties
* updated jolokia-core library to version 1.7.1

### Release 0.7.0 - 2019-11-10

* automatic reload of agent settings on configuration change
* start agent immediately for proper discovery

### Release 0.6.0 - 2019-09-20

* advanced configuration over REST API persisted to database
* version/info returns Atlassian/JIRA/7.13.5
* automated testing of multiple products (minimal/actual versions)

### Release 0.5.0 - 2019-02-11

* updated AMPS to 8.0
* automated testing for REST calls - initial
* automated testing for Jolokia calls - initial

### Release 0.4.2 - 2019-02-10

* support POST request to /jolokia path

### Release 0.4.1 - 2019-01-23

* updated jolokia-core library to version 1.6.0

### Release 0.4.0 - 2018-03-15

* updated documentation
* updated Jolokia library to version 1.3.7
* added warning about required restart in PUT response

### Release 0.3.0 - 2016-05-21

* marked for data center compatibility
* updated Jolokia library to version 1.3.2
* use built-in jolokia-access.xml when no other defined

### Release 0.2 - 2015-04-07

* initial public release

